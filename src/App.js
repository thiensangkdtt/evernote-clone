import './App.css';
import Navbar from './components/layout/Navbar'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Home from './components/home/Home'
import Favorites from './components/notes/Favorites';
import NoteDetail from "./components/notes/NoteDetail";
import EditForm from "./components/notes/EditForm";

function App() {
  return (
    <Router>
      <Navbar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/favorites" component={Favorites} />
        <Route exact path="/notes/:note_id" component={NoteDetail} />
        <Route exact path="/editform/:note_id" component={EditForm} />
      </Switch>
    </Router>
  );
}

export default App;
