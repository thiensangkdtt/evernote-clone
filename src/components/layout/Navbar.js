import React from 'react'
import { Link, NavLink, withRouter } from 'react-router-dom'

const Navbar = () => {
    return (
        <div>            
            <nav>
                <div className="nav-wrapper">
                <Link to="/" className="brand-logo right">Notebook</Link>
                    <ul id="nav-mobile" className="left hide-on-med-and-down">
                        <li><Link to="/favorites">Favorites</Link></li>
                    </ul>
                </div>
            </nav>
        </div>
    )
}

export default withRouter(Navbar)
