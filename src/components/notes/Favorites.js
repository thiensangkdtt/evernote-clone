import React from 'react'
import { useSelector } from 'react-redux'
import { useFirestoreConnect } from 'react-redux-firebase'
import NodeList from './noteList'

const Favorites = () => {
    useFirestoreConnect([{ 
            collection: "notes", 
            where: ["favorites", "==", true], 
            orderBy: ["createAt", "desc"], 
            storeAs: 'favnotes'
        }
    ])

    const favnotes = useSelector(state => state.firestore.ordered.favnotes)
    console.log(favnotes)
    return (<NodeList notes = {favnotes} />)
}

export default Favorites
