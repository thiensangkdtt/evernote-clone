import React from 'react'
import { deleteNote, toggleFav } from '../../store/actions/noteAction'
import {useDispatch} from 'react-redux'
import moment from "moment"
import { Link } from 'react-router-dom'

const Node = ({note}) => {
    const dispatch = useDispatch()
    const deleteNoteHandler = () => {
        dispatch(deleteNote(note))
    }

    const updateNoteHandler = () => {
        dispatch(toggleFav(note))
    }

    const editNoteHandler = () => {
        dispatch({type: "EDIT", payload: note})
    }

    const favostatus = note.favorites ? "Favoriting" : "Favorites"
    return (
        <div className="note white">
            <div className="right-align">
                <i className="material-icons red-text" style={{cursor: "pointer"}} onClick={updateNoteHandler}>{favostatus}</i>
                <p></p>
                <i className="material-icons" style={{cursor: "pointer"}} onClick={deleteNoteHandler}>delete</i>
            </div>
            <Link to={`/notes/${note.id}`}>Details</Link>
            <h5 className="black-text">{note.title}</h5>
            <p className="truncate">{note.content}</p>
            <p className="grey-text">{moment(note.createAt.toDate()).fromNow()}</p>
            <div className="right-align">
                <Link to={`/editform/${note.id}`}>
                    <i className="material-icons black-text" onClick={editNoteHandler}>Edit</i>
                </Link>
            </div>
        </div>
    )
}

export default Node
