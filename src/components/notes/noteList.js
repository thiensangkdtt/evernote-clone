import React from 'react'
import Node from './Node'

const noteList = ({notes}) => {
    return (
        <div className="nodelist">
            {notes && notes.map(note => 
                <Node note={note} key={note.id} />
            )}
        </div>
    )
}

export default noteList
