import firebase from 'firebase/compat/app';

const firebaseConfig = {
  apiKey: "AIzaSyA9YUcHzV7Cf1WBXpdrwMUOHxb6DcyvBa0",
  authDomain: "evernote-clone-f5565.firebaseapp.com",
  projectId: "evernote-clone-f5565",
  storageBucket: "evernote-clone-f5565.appspot.com",
  messagingSenderId: "333705534392",
  appId: "1:333705534392:web:40bde8e18efba37b640d4d"
};

firebase.initializeApp(firebaseConfig)

export default firebase
