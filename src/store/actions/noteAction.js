export const addNote = (note) => {
    return (dispatch, getState, {getFirestore}) => {
        const firestore = getFirestore()
        firestore.collection('notes')
        .add({
            ...note,
            favorites:false,
            createAt: new Date()
        })
        .then(() => {
            console.log("add the note successfully")
        })
        .catch(err =>{
            console.log(err)
        })
    }  
}

export const deleteNote = (note) => {
    return (dispatch, getState, {getFirestore}) => {
        const firestore = getFirestore()
        firestore.collection('notes').doc(note.id).delete()
        .then(() => {
            console.log("delete the note successfully")
        })
        .catch(err =>{
            console.log(err)
        })
    }  
}

export const toggleFav = (note) => {
    return (dispatch, getState, {getFirestore}) => {
        const favostatus = !note.favorites
        const firestore = getFirestore()
        firestore.collection('notes').doc(note.id).update({ 
            favorites: favostatus
        })
        .then(() => {
            console.log("update the note successfully")
        })
        .catch(err =>{
            console.log(err)
        })
    }  
}

export const updateNote = (note) => {
    return (dispatch, getState, {getFirestore}) => {
        const firestore = getFirestore()
        firestore.collection('notes').doc(note.id).update({ 
            title: note.title,
            content: note.content,
        })
        .then(() => {
            console.log("update the note successfully")
        })
        .catch(err =>{
            console.log(err)
        })
    }  
}